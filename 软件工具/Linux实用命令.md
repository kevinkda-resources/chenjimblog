# Linux实用命令


>记录个人常用配置

-----

- 禁用Vim退出后回显或者清屏  
在 `~/.bashrc` 添加  
`export TERM=xterm`  


----



- 命令行终端忽略文件路径tab忽略大小写  
在 ` ~/.inputrc` 添加  
`set completion-ignore-case on`  

-----



- 终端命令单独另起一行  
修改  `~/.bashrc` 中 `PS1` 字段  
在 `\w` 后加 `\n`  

-----


相关文章  
- Git配置和常用命令  
  <https://blog.csdn.net/CSqingchen/article/details/105674924>
- 安卓软件开发常用命令集合  
  <https://blog.csdn.net/CSqingchen/article/details/108150595>  
